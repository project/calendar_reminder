(function($, Drupal) {
  Drupal.behaviors.calendarReminderDropdowns = {
    attach: (context, settings) => {
      $('button.calendar-reminder-button', context)
        .once('calendarReminderDropdowns')
        .on('click', e => {
          const $target = $(e.target);
          const $parent = $target.parent();
          const $dropdown = $parent.find('.calendar-reminder-dropdown__menu');
          const isExpanded = $target.attr('aria-expanded') === 'true';

          if (isExpanded) {
            // Close dropdown.
            $dropdown.css('display', 'none');
          } else {
            // Open dropdown.
            $dropdown.css('display', 'block');
          }

          $target.attr('aria-expanded', !isExpanded);
        });

      // Close dropdown whenever child links are clicked.
      $('.calendar-reminder-dropdown__menu a', context)
        .once('calendarReminderDropdownLinks')
        .on('click', e => {
          const $target = $(e.target);
          const $dropdownMenu = $target.parent();
          const $dropdownContainer = $dropdownMenu.parent();
          const $calendarButton = $dropdownContainer.find('button.calendar-reminder-button');

          $dropdownMenu.css('display', 'none');
          $calendarButton.attr('aria-expanded', false);
        });
    }
  }
})(jQuery, Drupal);
