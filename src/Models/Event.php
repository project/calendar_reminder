<?php

namespace Drupal\calendar_reminder\Models;

/**
 * Represents a single one-time event.
 */
class Event {

  /**
   * Event Title.
   *
   * @var string
   */
  protected $title;

  /**
   * Event Start Date.
   *
   * @var string
   */
  protected $startDate;

  /**
   * Event End Date.
   *
   * @var string
   */
  protected $endDate;


  /**
   * Event Details.
   *
   * @var string
   */
  protected $details;

  /**
   * Event Organizer.
   *
   * @var string
   */
  protected $organizer;

  /**
   * Event Timezone.
   *
   * @var string
   */
  protected $timezone;

  /**
   * Event Location.
   *
   * @var string
   */
  protected $location;

  /**
   * Constructs BaseCalendarProvider.
   *
   * @param string $title
   *   The event title.
   * @param string $startDate
   *   The start date.
   * @param string $endDate
   *   The end date.
   * @param string $details
   *   The event details.
   * @param string $organizer
   *   The event organizer.
   * @param string $timezone
   *   The event timezone.
   * @param string $location
   *   The event location.
   */
  public function __construct(
    $title,
    $startDate,
    $endDate = NULL,
    $details = NULL,
    $organizer = NULL,
    $timezone = NULL,
    $location = NULL) {

    $this->title = $title;
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->details = $details;
    $this->organizer = $organizer;
    $this->timezone = $timezone;
    $this->location = $location;
  }

  /**
   * Get the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Sets the title.
   *
   * @param string $title
   *   The new event title.
   *
   * @return Event
   *   The current instance.
   */
  public function setTitle(string $title) {
    $this->title = $title;

    return $this;
  }

  /**
   * Get the start date.
   *
   * @return string
   *   The start date.
   */
  public function getStartDate(): string {
    return $this->startDate ?? '';
  }

  /**
   * Sets the start date.
   *
   * @param string $startDate
   *   The new event start date.
   *
   * @return Event
   *   The current instance.
   */
  public function setStartDate(string $startDate) {
    $this->startDate = $startDate;

    return $this;
  }

  /**
   * Get the end date.
   *
   * @return string
   *   The end date.
   */
  public function getEndDate(): string {
    return $this->endDate ?? '';
  }

  /**
   * Sets the end date.
   *
   * @param string $endDate
   *   The new event end date.
   *
   * @return Event
   *   The current instance.
   */
  public function setEndDate(string $endDate) {
    $this->endDate = $endDate;

    return $this;
  }

  /**
   * Get the event details.
   *
   * @return string
   *   The event details.
   */
  public function getEventDetails(): string {
    return $this->details ?? '';
  }

  /**
   * Sets the event Details.
   *
   * @param string $eventDetails
   *   The new event details.
   *
   * @return Event
   *   The current instance.
   */
  public function setEventDetails(string $eventDetails) {
    $this->details = $eventDetails;

    return $this;
  }

  /**
   * Get the event organizer.
   *
   * @return string
   *   The event organizer.
   */
  public function getEventOrganizer(): string {
    return $this->organizer ?? '';
  }

  /**
   * Sets the event organizer.
   *
   * @param string $eventOrganizer
   *   The new event organizer.
   *
   * @return Event
   *   The current instance.
   */
  public function setEventOrganizer(string $eventOrganizer) {
    $this->organizer = $eventOrganizer;

    return $this;
  }

  /**
   * Get the event timezone.
   *
   * @return string
   *   The event timezone.
   */
  public function getEventTimezone(): string {
    return $this->timezone ?? '';
  }

  /**
   * Sets the event timezone.
   *
   * @param string $eventTimezone
   *   The new event timezone.
   *
   * @return Event
   *   The current instance.
   */
  public function setEventTimezone(string $eventTimezone) {
    $this->timezone = $eventTimezone;

    return $this;
  }

  /**
   * Get the event location.
   *
   * @return string
   *   The event location.
   */
  public function getEventLocation(): string {
    return $this->location ?? '';
  }

  /**
   * Sets the event location.
   *
   * @param string $eventLocation
   *   The new event location.
   *
   * @return Event
   *   The current instance.
   */
  public function setEventLocation(string $eventLocation) {
    $this->location = $eventLocation;

    return $this;
  }

}
