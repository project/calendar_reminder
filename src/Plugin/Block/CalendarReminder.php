<?php

namespace Drupal\calendar_reminder\Plugin\Block;

use DateTimeZone;
use Drupal\calendar_reminder\Models\Event;
use Drupal\calendar_reminder\Providers\GoogleCalendarProvider;
use Drupal\calendar_reminder\Providers\ICalendarProvider;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'CalendarReminder' block.
 *
 * @Block(
 *   id = "calendar_reminder_block",
 *   admin_label = @Translation("Calendar Reminder Button"),
 *   category = @Translation("Calendar Reminder")
 * )
 */
class CalendarReminder extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The default timezone configured under Regional Settings.
   *
   * @var string
   */
  protected $defaultTimezone;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    /** @var \Drupal\Core\Config\ConfigFactory $configFactory */
    $configFactory = $container->get('config.factory');
    $systemDateConfig = $configFactory->get('system.date');
    $instance->defaultTimezone = $systemDateConfig->get('timezone')['default'] ?? 'UTC';

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'button_text' => '',
      'start_date' => '',
      'end_date' => '',
      'event_title' => '',
      'event_details' => [
        'value' => '',
        'format' => 'basic_html',
      ],
      'event_organizer' => '',
      'event_timezone' => '',
      'event_location' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#default_value' => $this->configuration['button_text'],
      '#required' => TRUE,
    ];

    $form['event_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Event Information'),
    ];

    $form['event_information']['event_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Title'),
      '#default_value' => $this->configuration['event_title'],
      '#required' => TRUE,
    ];

    $form['event_information']['start_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Event Start Date'),
      '#date_increment' => 60,
      '#date_timezone' => $this->defaultTimezone,
      '#date_year_range' => '-10:+10',
      '#default_value' => $this->configuration['start_date'],
    ];

    $form['event_information']['end_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Event End Date'),
      '#date_increment' => 60,
      '#date_timezone' => $this->defaultTimezone,
      '#date_year_range' => '-10:+10',
      '#default_value' => $this->configuration['end_date'],
    ];

    $form['event_information']['event_details'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Event Details'),
      '#format' => $this->configuration['event_details']['format'] ?? 'basic_html',
      '#default_value' => $this->configuration['event_details']['value'] ?? '',
    ];

    $form['event_information']['event_organizer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Organizer'),
      '#default_value' => $this->configuration['event_organizer'],
    ];

    $form['event_information']['event_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Location'),
      '#default_value' => $this->configuration['event_location'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['button_text'] = $values['button_text'] ?? '';
    $this->configuration['event_title'] = $values['event_information']['event_title'] ?? '';
    $this->configuration['start_date'] = $values['event_information']['start_date'] ?? '';
    $this->configuration['end_date'] = $values['event_information']['end_date'] ?? '';
    $this->configuration['event_details']['value'] = $values['event_information']['event_details']['value'] ?? '';
    $this->configuration['event_details']['format'] = $values['event_information']['event_details']['format'] ?? '';
    $this->configuration['event_organizer'] = $values['event_information']['event_organizer'] ?? '';
    $this->configuration['event_location'] = $values['event_information']['event_location'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $build = [
      '#theme' => 'calendar_reminder',
      '#button_text' => $this->configuration['button_text'] ?? $this->t('Add Reminder'),
      '#event_title' => $this->configuration['event_title'],
      '#attached' => [
        'library' => [
          'calendar_reminder/base_styles',
          'calendar_reminder/dropdown',
        ],
      ],
    ];

    $startDate = '';
    $endDate = '';

    if ($this->configuration['start_date']) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime */
      $startDateDrupalDateTime = $this->configuration['start_date'];
      $startDateTime = $startDateDrupalDateTime->getPhpDateTime();
      $startDateTime->setTimezone(new DateTimeZone($this->defaultTimezone));
      $startDate = date('Ymd\\THi00\\Z', $startDateTime->getTimestamp());
    }

    if ($this->configuration['end_date']) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime */
      $endDateDrupalDateTime = $this->configuration['end_date'];
      $endDateTime = $endDateDrupalDateTime->getPhpDateTime();
      $endDateTime->setTimezone(new DateTimeZone($this->defaultTimezone));
      $endDate = date('Ymd\\THi00\\Z', $endDateTime->getTimestamp());
    }

    $event = new Event(
      $this->configuration['event_title'],
      $startDate,
      $endDate,
      $this->configuration['event_details']['value'],
      $this->configuration['event_organizer'],
      $this->defaultTimezone,
      $this->configuration['event_location'],
    );

    $gcalLink = GoogleCalendarProvider::encode($event);
    $icalLink = ICalendarProvider::encode($event);

    $build['#gcal_link'] = !empty($gcalLink) ? $gcalLink : NULL;
    $build['#ical_link'] = !empty($icalLink) ? $icalLink : NULL;

    // Construct unique identifier.
    $uniqueId = str_replace(' ', '-', $this->configuration['event_title']);
    $uniqueId .= date('Y-m-d');
    $build['#unique_id'] = $uniqueId;

    return $build;
  }

}
