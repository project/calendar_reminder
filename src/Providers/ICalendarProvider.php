<?php

namespace Drupal\calendar_reminder\Providers;

use Drupal\calendar_reminder\Models\Event;

/**
 * Provides ICalendar support.
 */
class ICalendarProvider extends BaseCalendarProvider {

  /**
   * Event Start Date Key.
   *
   * @var string
   */
  protected static $startDateKey = 'DTSTART';

  /**
   * Event End Date Key.
   *
   * @var string
   */
  protected static $endDateKey = 'DTEND';

  /**
   * Event Title Key.
   *
   * @var string
   */
  protected static $eventTitleKey = 'SUMMARY';

  /**
   * Event Details Key.
   *
   * @var string
   */
  protected static $eventDetailsKey = 'DESCRIPTION';

  /**
   * Event Organizer Key.
   *
   * @var string
   */
  protected static $eventOrganizerKey = 'ORGANIZER';

  /**
   * Event Timezone Key.
   *
   * @var string
   */
  protected static $eventTimezoneKey = 'TZID';

  /**
   * Event Location Key.
   *
   * @var string
   */
  protected static $eventLocationKey = 'LOCATION';

  /**
   * {@inheritDoc}
   */
  public static function encode(Event $event) {
    $encodedCalendar = "data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT";
    $eventArgs = [];

    $eventArgs[self::$eventTitleKey] = $event->getTitle();
    $eventArgs[self::$startDateKey] = $event->getStartDate();
    $eventArgs[self::$endDateKey] = $event->getEndDate();
    $eventArgs[self::$eventDetailsKey] = $event->getEventDetails();
    $eventArgs[self::$eventOrganizerKey] = $event->getEventOrganizer();
    $eventArgs[self::$eventLocationKey] = $event->getEventLocation();
    $eventArgs[self::$eventTimezoneKey] = $event->getEventTimezone();

    $eventArgs = array_filter($eventArgs);
    if (empty($eventArgs)) {
      return '';
    }

    $encodedEvent = '';
    foreach ($eventArgs as $key => $value) {
      if ($key === self::$eventDetailsKey) {
        $encodedEvent .= '%0D%0A' . $key . ':' . strip_tags($value);
        // Append html details for calendars that support it.
        $encodedEvent .= '%0D%0AX-ALT-DESC;FMTTYPE=text/html:';
        $encodedEvent .= '<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 3.2//EN""><HTML><BODY>' . $value . '</BODY></HTML>.';
      }
      else {
        $encodedEvent .= '%0D%0A' . $key . ':' . $value;
      }
    }

    $encodedCalendar .= $encodedEvent;
    $encodedCalendar .= '%0AEND:VEVENT%0AEND:VCALENDAR';

    return $encodedCalendar;
  }

}
