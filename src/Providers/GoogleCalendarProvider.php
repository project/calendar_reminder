<?php

namespace Drupal\calendar_reminder\Providers;

use Drupal\calendar_reminder\Models\Event;
use Drupal\Component\Utility\UrlHelper;

/**
 * Provides Google Calendar support.
 */
class GoogleCalendarProvider extends BaseCalendarProvider {

  /**
   * Event Title Key.
   *
   * @var string
   */
  protected static $eventTitleKey = 'text';

  /**
   * Event Details Key.
   *
   * @var string
   */
  protected static $eventDetailsKey = 'details';

  /**
   * Event Organizer Key.
   *
   * @var string
   */
  protected static $eventOrganizerKey = 'organizer';

  /**
   * Event Timezone Key.
   *
   * @var string
   */
  protected static $eventTimezoneKey = 'ctz';

  /**
   * Event Location Key.
   *
   * @var string
   */
  protected static $eventLocationKey = 'location';

  /**
   * {@inheritDoc}
   */
  public static function encode(Event $event) {
    $baseUrl = 'https://www.google.com/calendar/r/eventedit';
    $queryArgs = [];

    $queryArgs[self::$eventTitleKey] = $event->getTitle();
    $queryArgs[self::$eventDetailsKey] = $event->getEventDetails();
    $queryArgs[self::$eventOrganizerKey] = $event->getEventOrganizer();
    $queryArgs[self::$eventLocationKey] = $event->getEventLocation();
    $queryArgs[self::$eventTimezoneKey] = $event->getEventTimezone();

    $queryArgs = array_filter($queryArgs);

    if ($event->getStartDate()) {
      $queryArgs['dates'] = $event->getStartDate();

      if ($event->getEndDate()) {
        $queryArgs['dates'] .= '/' . $event->getEndDate();
      }
    }

    $encodedQueryArgs = UrlHelper::buildQuery($queryArgs);

    if (!empty($encodedQueryArgs)) {
      return $baseUrl . '?' . $encodedQueryArgs;
    }

    return '';
  }

}
