<?php

namespace Drupal\calendar_reminder\Providers;

use Drupal\calendar_reminder\Models\Event;

/**
 * Base Calendar Provider class.
 */
abstract class BaseCalendarProvider {

  /**
   * Event Start Date Key.
   *
   * @var string
   */
  protected static $startDateKey = '';

  /**
   * Event End Date Key.
   *
   * @var string
   */
  protected static $endDateKey = '';

  /**
   * Event Title Key.
   *
   * @var string
   */
  protected static $eventTitleKey = '';

  /**
   * Event Details Key.
   *
   * @var string
   */
  protected static $eventDetailsKey = '';

  /**
   * Event Organizer Key.
   *
   * @var string
   */
  protected static $eventOrganizerKey = '';

  /**
   * Event Timezone Key.
   *
   * @var string
   */
  protected static $eventTimezoneKey = '';

  /**
   * Event Location Key.
   *
   * @var string
   */
  protected static $eventLocationKey = '';

  /**
   * Encodes the given event information into the appropriate format.
   *
   * @param \Drupal\calendar_reminder\Models\Event $event
   *   The event information to encode.
   *
   * @return string
   *   The encoded data.
   */
  abstract public static function encode(Event $event);

}
